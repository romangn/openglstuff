// GLEW
#define GLEW_STATIC
#include<GL\glew.h>
// GLFW
#include<GLFW\glfw3.h>
#include<iostream>
#include "Shader.h"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;

int main()
{
	// initialize GLFW
	glfwInit();

	// setup the required options
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// create a GLFWwindow object
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
	if (!window)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimension
	int width, height; // these will be overwritten OUT
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	// Set the required callback functionality
	glfwSetKeyCallback(window, key_callback);

	// Creating and compiling shaders and program
	Shader ourShader("D:/OpenGL/OpenGLStuff/Shaders/Vertex.txt",
		"D:/OpenGL/OpenGLStuff/Shaders/Fragment.txt");

	// Set up vertex data (and buffer(s)) and attribute pointers

	// THIS IS FOR TRIANGLE
	GLfloat vertices[] = {
		// Position			// Color
		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Bottom right
		0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // Bottom left
		0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f // Top
	};

	GLuint VBO, VAO;
	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);
	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO); // subsequent calls after bindings the VAO are stored inside it
							// copy our vertices array in a buffer for OpenGl to use
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// then set our vertex attribute pointers
	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	// Color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat))); // Last parameter is offset
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind
									  // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)
	glBindVertexArray(0);

	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // UNCOMMENT TO HAVE A LOOK AT WIREFRAME MODE

	// program loop
	while (!glfwWindowShouldClose(window))
	{
		// check all call events
		glfwPollEvents();

		// rendering commands here
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT); // clear the color buffer

		// update the color each cycle
		// GLfloat timeValue = glfwGetTime();
		// GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
		// GLint vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
		ourShader.Use(); // shader HAS to be activated
		// glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);

		// Now draw the triangle
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindVertexArray(0);
		// swap the buffers, shoud be done in the end
		glfwSwapBuffers(window);
	}
	// Deallocate the resources 
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	// Terminate glfw
	glfwTerminate();
	return 0;
}

// Called whenever the ESCAPE key is pressed
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	// when a user presses the escape key, we set the WindowShouldClose property to true
	// closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}